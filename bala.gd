extends Area2D

const SPEED = 100
var velocity = Vector2()
var norantza = 1

func set_norantza(nor):
    norantza = nor
    if norantza == 1:
        $AnimatedSprite.flip_h = false
    else:
        $AnimatedSprite.flip_h = true

func _physics_process(delta):
    velocity.x = SPEED * delta * norantza
    translate(velocity)
    $AnimatedSprite.play("default")

func _on_bala_body_entered(body):
    if "etsaiak" in body.get_groups():
        print(body.name)
        body.hil()
        queue_free()
