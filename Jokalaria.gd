extends KinematicBody2D

const GRABITATEA = 10
const BALA = preload("res://bala.tscn")
var norantza = 1
var energia = 3
export (int) var speed = 200
export (int) var salto = -300

var velocity = Vector2()


func get_input():
    velocity.x = 0
    if Input.is_action_pressed('Right'):
        velocity.x = speed
        $AnimatedSprite.flip_h = false
        norantza = 1
    if Input.is_action_pressed('Left'):
        velocity.x = -speed
        $AnimatedSprite.flip_h = true
        norantza = -1
    if Input.is_action_just_pressed('fire'):    
        var bala = BALA.instance()
        get_parent().add_child(bala)
        bala.set_norantza(norantza)
        bala.position = $Position2D.global_position
    if Input.is_action_pressed('Up') and is_on_floor():
            velocity.y = salto

func _physics_process(delta):
    get_input()
    velocity.y += GRABITATEA
    if velocity.x != 0:
        $AnimatedSprite.play("ibili")
    else:
        $AnimatedSprite.play("geldi")
    move_and_slide(velocity,Vector2( 0, -1 ))
    eguneratu_energia()

func eguneratu_energia():
    get_tree().get_root().get_node("Nagusia").find_node("Energia").set_text("Energia: " + str(energia))
    
    
func mina_jaso(mina=1):
    energia -= mina
    if energia < 1:
        queue_free()
        get_tree().quit()